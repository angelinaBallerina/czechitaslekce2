package com.example.czechitaslekce2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView hlavniTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hlavniTextView = findViewById(R.id.hlavniTextView);
        hlavniTextView.setText("Zkouska");

        Button tlacitko = findViewById(R.id.tlaciko);
        tlacitko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this,
                //        "prvni toast", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this,
                        SecondActivity.class);
                intent.putExtra("DATA",
                        "Posilame data z jedne aktivity do druhe");
                MainActivity.this.startActivity(intent);
            }
        });
    }

}
